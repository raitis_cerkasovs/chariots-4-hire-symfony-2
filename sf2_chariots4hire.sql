-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 17, 2014 at 03:04 PM
-- Server version: 5.5.37-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sf2_chariots4hire`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` longtext,
  `title` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  `autoslag` longtext,
  `slag` varchar(255) DEFAULT NULL,
  `slagtrue` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `body`, `title`, `thumbnail`, `active`, `publish_date`, `temp1`, `temp2`, `temp3`, `autoslag`, `slag`, `slagtrue`) VALUES
(1, 'You can edit this page by clicking "Main menu" in menu.', 'Chariots4hire', NULL, NULL, '2012-04-12 11:07:35', NULL, NULL, NULL, 'auto-slag-main-menu', 'mini-bus-hire', 1),
(2, '<p>\r\n	<span class="Normal-C">Chariots4hire is a family run business which was established in 1991. We are a fully insured and licensed Public Service Vehicle (PSV) Operator. All our minibuses are fitted with seat belts and maintained to a very high standard.</span><br />\r\n	<span class="Normal-C">We are situated in close proximity to Stansted airport, Harlow and Bishop&#39;s Stortford.</span><br />\r\n	<br />\r\n	<span class="Normal-C">Our chauffeur driven minibuses for up to <span style="color: rgb(0, 0, 0);">16 passengers</span> are available for private, contract or corporate work of any description.</span><br />\r\n	<br />\r\n	<span class="Normal-C">Typical services include trips to restaurants, theatres, pubs and clubs, sporting venues such as racing, football and cricket, airports and corporate functions.</span><br />\r\n	<br />\r\n	<span class="Normal-C">So whatever the venue click here for an <a href="/newQuote"><span style="color: rgb(255, 0, 0);">online quote</span></a>&nbsp;</span><br />\r\n	<span class="Normal-C">or call for a Chariot on......01279 724020</span></p>', 'vvAbout us || phone  number || opening hours', NULL, NULL, '2012-04-12 11:08:52', NULL, NULL, NULL, 'chariots4hire-is-a-family-run-business-which-was-established-in-1991-we-are-a-fully-insured-and-lice', 'chariots-opening-hours', 1),
(3, '<h2>\r\n	Harlow</h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	You can edit this page by clicking &quot;Bus Hire in Harlow&quot; in menu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<table border="0" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<img alt="" src="/ckfinder/userfiles/images/197605_197764416915123_130541460304086_616674_1632617_n.jpg" style="width: 200px; height: 133px;" /></td>\r\n			<td>\r\n				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra justo eu est tempus vel bibendum turpis sollicitudin. Donec dapibus elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan="2">\r\n				<p>\r\n					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra justo eu est tempus vel bibendum turpis sollicitudin. Donec dapibus elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas dapibus eros non nisl varius vel venenatis mi sodales. Nam suscipit lorem id dui vestibulum hendrerit. Phasellus sit amet tortor non mauris volutpat condimentum. Cras sed nibh ipsum, sit amet sodales sapien. Nullam gravida venenatis luctus.</p>\r\n				<p>\r\n					&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>', 'Mini bus Hire in Harlow', NULL, NULL, '2012-04-16 19:49:15', NULL, NULL, NULL, 'harlow-you-can-edit-this-page-by-clicking-bus-hire-in-harlow-in-menu-lorem-ipsum-dolor-sit-amet-cons', 'mini-bus-hire-in-harlow', 1),
(4, '<h2>\r\n	Sawbridgeworth</h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	You can edit this page by clicking &quot;Bus Hire in Sawbridgeworth&quot; in menu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<table border="0" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<img alt="" src="/ckfinder/userfiles/images/197605_197764416915123_130541460304086_616674_1632617_n.jpg" style="width: 200px; height: 133px;" /></td>\r\n			<td>\r\n				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra justo eu est tempus vel bibendum turpis sollicitudin. Donec dapibus elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan="2">\r\n				<p>\r\n					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra justo eu est tempus vel bibendum turpis sollicitudin. Donec dapibus elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas dapibus eros non nisl varius vel venenatis mi sodales. Nam suscipit lorem id dui vestibulum hendrerit. Phasellus sit amet tortor non mauris volutpat condimentum. Cras sed nibh ipsum, sit amet sodales sapien. Nullam gravida venenatis luctus.</p>\r\n				<p>\r\n					&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>', 'Mini bus Hire in Sawbridgeworth', NULL, NULL, '2012-04-16 22:25:08', NULL, NULL, NULL, 'sawbridgeworth-you-can-edit-this-page-by-clicking-bus-hire-in-sawbridgeworth-in-menu--lorem-ipsum-do', 'mini-bus-hire-in-sawbridgeworth', 1),
(5, '<h2>\r\n	B&#39;Stortford</h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	You can edit this page by clicking &quot;Bus Hire in B&#39;Stortford&quot; in menu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<table border="0" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<img alt="" src="/ckfinder/userfiles/images/197605_197764416915123_130541460304086_616674_1632617_n.jpg" style="width: 200px; height: 133px;" /></td>\r\n			<td>\r\n				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra justo eu est tempus vel bibendum turpis sollicitudin. Donec dapibus elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan="2">\r\n				<p>\r\n					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra justo eu est tempus vel bibendum turpis sollicitudin. Donec dapibus elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas dapibus eros non nisl varius vel venenatis mi sodales. Nam suscipit lorem id dui vestibulum hendrerit. Phasellus sit amet tortor non mauris volutpat condimentum. Cras sed nibh ipsum, sit amet sodales sapien. Nullam gravida venenatis luctus.</p>\r\n				<p>\r\n					&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>', 'Mini bus hire in B''Strotford', NULL, NULL, '2012-04-16 22:28:06', NULL, NULL, NULL, 'b-39-stortford-you-can-edit-this-page-by-clicking-bus-hire-in-b-39-stortford-in-menu--lorem-ipsum-do', 'mini-bus-hire-in-b-strotford', 1),
(6, '<p>\r\n	You can edit this page by dgdg.</p>\r\n<p>\r\n	You can edit this page by dgdg.</p>\r\n<p>\r\n	You can edit this page by dgdg.</p>\r\n<p>\r\n	You can edit this page by dgdg.</p>\r\n<p>\r\n	You can edit this page by dgdg.</p>\r\n<p>\r\n	You can edit this page by dgdg.</p>\r\n<p>\r\n	You can edit this page by dgdg.</p>\r\n<p>\r\n	You can edit this page by dgdg.</p>\r\n<p>\r\n	You can edit this page by dgdg.</p>\r\n<p>\r\n	You can edit this page by dgdg.</p>\r\n<p>\r\n	You can edit this page by dgdg.</p>\r\n<p>\r\n	You can edit this page by dgdg.</p>', 'Chariots 4 hire', NULL, NULL, '2012-04-18 19:46:10', NULL, NULL, NULL, 'you-can-edit-this-page-by-dgdg-you-can-edit-this-page-by-dgdg-you-can-edit-this-page-by-dgdg-you-can', 'coopersale-van-hire', 1),
(7, '<h2>\r\n	Hertford</h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	You can edit this <a href="http://test.chariots4hire.co.uk/some-texttexttexttexttexttexttexttext/3/show">gpa</a>e by clicking &quot;Bus Hire in Hertford&quot; in menu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<table border="0" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<img alt="" src="/ckfinder/userfiles/images/197605_197764416915123_130541460304086_616674_1632617_n.jpg" style="width: 200px; height: 133px;" /></td>\r\n			<td>\r\n				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra justo eu est tempus vel bibendum turpis sollicitudin. Donec dapibus elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan="2">\r\n				<p>\r\n					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra justo eu est tempus vel bibendum turpis sollicitudin. Donec dapibus elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas dapibus eros non nisl varius vel venenatis mi sodales. Nam suscipit lorem id dui vestibulum hendrerit. Phasellus sit amet tortor non mauris volutpat condimentum. Cras sed nibh ipsum, sit amet sodales sapien. Nullam gravida venenatis luctus.</p>\r\n				<p>\r\n					&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>', 'Mini bus hire in Hertford', NULL, NULL, '2012-04-19 21:21:42', NULL, NULL, NULL, 'hertford-you-can-edit-this-gpae-by-clicking-bus-hire-in-hertford-in-menu--lorem-ipsum-dolor-sit-amet', 'mini-bus-hire-in-hertford', 1),
(8, '<h2>\r\n	Cheshunt</h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	You can edit this page by clicking &quot;Bus Hire in Ceshunt&quot; in menu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<table border="0" cellpadding="1" cellspacing="1" style="width: 500px;">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<img alt="" src="/ckfinder/userfiles/images/197605_197764416915123_130541460304086_616674_1632617_n.jpg" style="width: 200px; height: 133px;" /></td>\r\n			<td>\r\n				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra justo eu est tempus vel bibendum turpis sollicitudin. Donec dapibus elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan="2">\r\n				<p>\r\n					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra justo eu est tempus vel bibendum turpis sollicitudin. Donec dapibus elementum blandit. Ut bibendum tortor sit amet lectus pulvinar volutpat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas dapibus eros non nisl varius vel venenatis mi sodales. Nam suscipit lorem id dui vestibulum hendrerit. Phasellus sit amet tortor non mauris volutpat condimentum. Cras sed nibh ipsum, sit amet sodales sapien. Nullam gravida venenatis luctus.</p>\r\n				<p>\r\n					&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>', 'Mini bus hire in Ceshunt', NULL, NULL, '2012-04-19 21:22:11', NULL, NULL, NULL, 'cheshunt-you-can-edit-this-page-by-clicking-bus-hire-in-ceshunt-in-menu--lorem-ipsum-dolor-sit-amet-', 'mini-bus-hire-in-ceshunt', 1),
(9, '<p>\r\n	You can edit this page by clicking &quot;test&quot; in menu. jheoiyteoi4th</p>\r\n<p>\r\n	r</p>\r\n<p>\r\n	y6</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	u</p>\r\n<p>\r\n	tut</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	ut</p>\r\n<p>\r\n	u</p>', 'test title', NULL, NULL, '2012-05-01 12:03:00', NULL, NULL, NULL, 'you-can-edit-this-page-by-clicking-test-in-menu-jheoiyteoi4th-r-y6-u-tut-ut-u', 'test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `articleout`
--

CREATE TABLE IF NOT EXISTS `articleout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` longtext,
  `title` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `locale` varchar(10) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `autoslag` longtext,
  `slag` varchar(255) DEFAULT NULL,
  `slagtrue` tinyint(1) DEFAULT NULL,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `articleout`
--

INSERT INTO `articleout` (`id`, `body`, `title`, `name`, `locale`, `thumbnail`, `active`, `publish_date`, `autoslag`, `slag`, `slagtrue`, `temp1`, `temp2`, `temp3`) VALUES
(1, '<p>\r\n	test</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>', 'test', 'test', 'gb', NULL, NULL, NULL, 'test-------', '', 0, NULL, NULL, NULL),
(3, '<p>\r\n	some</p>\r\n<p>\r\n	texttexttexttexttexttexttexttext</p>', 'buses', 'xx', 'gb', NULL, NULL, NULL, 'some-texttexttexttexttexttexttexttext', 'xbases', 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cat1`
--

CREATE TABLE IF NOT EXISTS `cat1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `langcode` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `design` longtext,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  PRIMARY KEY (`id`),
  KEY `IDX_5B4F9A2E7294869C` (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `cat1`
--

INSERT INTO `cat1` (`id`, `article_id`, `langcode`, `name`, `active`, `position`, `design`, `temp1`, `temp2`, `temp3`) VALUES
(29, 1, 'gb', 'Main menu', 1, 1, NULL, NULL, NULL, NULL),
(30, 2, 'gb', 'Second main', 1, 2, NULL, NULL, NULL, NULL),
(31, 3, 'gb', 'Bus Hire in Harlow', 1, 3, NULL, NULL, NULL, NULL),
(32, 4, 'gb', 'Bus Hire in Sawbridgeworth', 1, 5, NULL, NULL, NULL, NULL),
(33, 5, 'gb', 'Mini bus hire in B''Strotford', 1, 4, NULL, NULL, NULL, NULL),
(35, 7, 'gb', 'Mini bus hire in Hertford', 1, 6, NULL, NULL, NULL, NULL),
(36, 8, 'gb', 'Mini bus hire in Ceshunt', 1, 7, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cat2`
--

CREATE TABLE IF NOT EXISTS `cat2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat1_id` int(11) DEFAULT NULL,
  `ajaxtrue` tinyint(1) DEFAULT NULL,
  `langcode` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `design` longtext,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  `article_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C246CB94395F9010` (`cat1_id`),
  KEY `IDX_C246CB947294869C` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cat3`
--

CREATE TABLE IF NOT EXISTS `cat3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat2_id` int(11) DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `ajaxtrue` tinyint(1) DEFAULT NULL,
  `langcode` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `design` longtext,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  PRIMARY KEY (`id`),
  KEY `IDX_B541FB022BEA3FFE` (`cat2_id`),
  KEY `IDX_B541FB027294869C` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `body` longtext,
  `title` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  `flag` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `name`, `path`, `body`, `title`, `thumbnail`, `active`, `publish_date`, `temp1`, `temp2`, `temp3`, `flag`) VALUES
(5, NULL, NULL, NULL, NULL, NULL, NULL, '2030-04-14 14:52:17', NULL, NULL, NULL, 1),
(13, NULL, '4f9077d2c5f07.jpg', 'Our chauffeur driven minibuses for up to 16 passengers are available for private, contract or corporate work of any description', 'Mini bus hire', NULL, NULL, '2012-04-19 20:38:42', NULL, NULL, NULL, 1),
(14, NULL, '4f9079e54212c.jpg', 'Let our friendly staff take care of your journey to and from your event and all you have to do is enjoy your night out!', 'Make your night out simple', NULL, NULL, '2012-04-19 20:47:33', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `fos_user`
--

CREATE TABLE IF NOT EXISTS `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `username_canonical` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_canonical` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `algorithm` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `address` longtext,
  `active` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `design` longtext,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `algorithm`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `name`, `surname`, `address`, `active`, `position`, `design`, `temp1`, `temp2`, `temp3`) VALUES
(31, 'admin', 'admin', 'admin@chariots4hire.co.uk', 'admin@chariots4hire.co.uk', 1, 'sha512', '754yhz9130kkokc88s800804sk0o04c', 'd425c83907a8b2345c051a6fc1a136fb195a33052ca4e1607a2c7f4d3172842dabc0b5d3978c7a3933402289dd02968185d3e691ebf35002835d9511fa8a30b3', '2014-12-17 14:33:00', 0, 0, NULL, NULL, '2012-04-19 21:02:50', 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, 'Nick', 'Thorn', 'London', NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'ray', 'ray', '66x@inbox.lv', '66x@inbox.lv', 1, 'sha512', 'gst138ne69wgs4gw0kok404ck408ocs', 'c913ecca8ddef8522622351f69ad41b59d76c3f145a34c1b4945dd8ee782b0a89dd0d2172c14e36b6b1418d323ae2b8d15d187e22a679779473e713a88256220', '2012-04-23 13:12:14', 0, 0, NULL, NULL, '2012-03-21 14:52:50', 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, 'Raitis', 'Čerkasovs', 'London', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lang`
--

CREATE TABLE IF NOT EXISTS `lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `lang`
--

INSERT INTO `lang` (`id`, `name`, `code`, `country`, `active`, `position`, `temp1`, `temp2`, `temp3`) VALUES
(46, 'GB', 'gb', 'GB', 1, 6, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quote`
--

CREATE TABLE IF NOT EXISTS `quote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `people` int(11) DEFAULT NULL,
  `transportDate` date DEFAULT NULL,
  `transportTime` time DEFAULT NULL,
  `point` varchar(255) DEFAULT NULL,
  `destination` varchar(255) DEFAULT NULL,
  `single` varchar(255) DEFAULT NULL,
  `returnDate` date DEFAULT NULL,
  `returnTime` time DEFAULT NULL,
  `points` longtext,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `info` longtext,
  `find` varchar(255) DEFAULT NULL,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `quote`
--

INSERT INTO `quote` (`id`, `name`, `people`, `transportDate`, `transportTime`, `point`, `destination`, `single`, `returnDate`, `returnTime`, `points`, `phone`, `email`, `info`, `find`, `temp1`, `temp2`, `temp3`) VALUES
(32, 'test2', 1, '2012-04-18', '00:00:00', 'test2', 'test2', 'Single', NULL, '00:00:00', NULL, NULL, 'test2@test2.uk', NULL, 'Yell.com', NULL, NULL, NULL),
(33, 'test3', 4, '2012-04-20', '00:00:00', 'test3', 'test3', 'Return', '2012-04-25', '00:00:00', NULL, 'test3', 'test3@test3.uk', NULL, 'Our website', NULL, NULL, NULL),
(34, 'test5', 1, '2012-04-20', '00:00:00', 'test4', 'test4', 'Return', NULL, '00:00:00', 'test4', 'test4', 'test4@test4.uk', NULL, 'Yell.com', NULL, NULL, NULL),
(35, 'test6', 1, '2012-04-20', '00:00:00', 'test4', 'test4', 'Return', NULL, '00:00:00', 'test4', 'test4', 'test4@test4.uk', NULL, 'Yell.com', NULL, NULL, NULL),
(36, 'test7', 1, '2012-04-20', '00:00:00', 'test4', 'test4', 'Return', NULL, '00:00:00', 'test4', 'test4', 'test4@test4.uk', NULL, 'Yell.com', NULL, NULL, NULL),
(37, 'ttest 1000', 1, '2012-05-15', '00:00:00', 'dg', 'dfg', 'Return', '2012-05-15', '00:00:00', 'ete', '345', 'aer@jutf.lv', 'aewe', 'Repeat booking', NULL, NULL, NULL),
(38, 'awd', 1, '2014-12-18', '00:00:00', '`sdawd', 'asfesfesf', 'Single', '2014-12-24', '00:00:00', 'ascacac', 'asca', 'ad@werg.lv', 'ascasca', 'Search engine', NULL, NULL, NULL),
(39, 'seyw5y', 1, '2014-12-18', '02:00:00', 'sefgwegfe', 'shtrht', 'Return', '2014-12-18', '00:00:00', 'afwf', '3456345646', '66x@inbox.lv', 'wEFWFE', 'Search engine', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `utility`
--

CREATE TABLE IF NOT EXISTS `utility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `temp1` varchar(255) DEFAULT NULL,
  `temp2` varchar(255) DEFAULT NULL,
  `temp3` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `utility`
--

INSERT INTO `utility` (`id`, `value`, `active`, `temp1`, `temp2`, `temp3`) VALUES
(1, 'Detect User Locale', 1, NULL, NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cat1`
--
ALTER TABLE `cat1`
  ADD CONSTRAINT `FK_5B4F9A2E7294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);

--
-- Constraints for table `cat2`
--
ALTER TABLE `cat2`
  ADD CONSTRAINT `FK_C246CB94395F9010` FOREIGN KEY (`cat1_id`) REFERENCES `cat1` (`id`),
  ADD CONSTRAINT `FK_C246CB947294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);

--
-- Constraints for table `cat3`
--
ALTER TABLE `cat3`
  ADD CONSTRAINT `FK_B541FB022BEA3FFE` FOREIGN KEY (`cat2_id`) REFERENCES `cat2` (`id`),
  ADD CONSTRAINT `FK_B541FB027294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
