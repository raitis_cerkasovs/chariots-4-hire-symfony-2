/*
 * © 2009
 *
 * Antra Alksnaja
 * antra@alksnaja.lv
 */


var jsw={active:false,init:function(){$j("#overlay").click(function(){jsw.hide();}).css('height',$j(document).height()).fadeTo(0,0.75);},open:function(e,c){if($j(e).length<1){return false;}jsw.active=e;$j("#overlay").show();$j(e).show();if(c){$j(c).click(function(){return jsw.hide();});}return false;},hide:function(){if(jsw.active==false){return false;}$j("#overlay").hide();$j(jsw.active).hide();jsw.active=false;return false;}}