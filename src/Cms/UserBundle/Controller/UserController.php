<?php

namespace Cms\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cms\UserBundle\Entity\User;
use Cms\UserBundle\Form\UserType;



/**
 * User controller.
 *
 */
class UserController extends Controller
{
    /**
     * Lists all User entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('CmsUserBundle:User')->findBy(array(), array('email' => 'asc')); 

        return $this->render('CmsUserBundle:User:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a User entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CmsUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CmsUserBundle:User:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new User entity.
     *
     */
    public function newAction()
    {
        $entity = new User();
        $form   = $this->createForm(new UserType(), $entity);

        return $this->render('CmsUserBundle:User:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new User entity.
     *
     */
    public function createAction()
    {
        $entity  = new User();
        $request = $this->getRequest();
        $form    = $this->createForm(new UserType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('crud_user_show', array('id' => $entity->getId())));
            
        }

        return $this->render('CmsUserBundle:User:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CmsUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User.');
        }

        $editForm = $this->createForm(new UserType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CmsUserBundle:User:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing User entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CmsUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm   = $this->createForm(new UserType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('crud_user'));
        }

        return $this->render('CmsUserBundle:User:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a User entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('CmsUserBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('crud_user'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
	
	
	
	//////////////////////////////////
	


    public function addRoleAction($id)
    {
		$em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository('CmsUserBundle:User')->find($id);			

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User.');
        }

        return $this->render('CmsUserBundle:User:addRole.html.twig', array(
            'entity'      => $entity,
        ));
	}
	
	

    public function removeRoleAction($id, $role)
    {
		$em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository('CmsUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User.');
        }
        
	    $temp = $entity->getRoles();
        foreach ($temp as $r => $value):
             if ($value == $role):
			     unset($temp[$r]);
			     $entity->setRoles(array_values($temp));
				   $em = $this->getDoctrine()->getEntityManager();
                   $em->persist($entity);
                   $em->flush();
			 endif;
		endforeach;


	   
	    return $this->redirect($this->generateUrl('crud_user_edit', array('id' => $id)));

	}	
	
	


    public function addRoleSaveAction($id)
    {
		$em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository('CmsUserBundle:User')->find($id);	
		$request = $this->getRequest();
		
		$entity->addRole($this->get('request')->request->get('choice'));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User.');
        }

				   $em = $this->getDoctrine()->getEntityManager();
                   $em->persist($entity);
                   $em->flush();

	    return $this->redirect($this->generateUrl('crud_user_edit', array('id' => $id)));

	}
	
	    public function welcomeRegAction()
    {
      return $this->render('CmsUserBundle:User:welcomeReg.html.twig');	
	}
	
	
}
