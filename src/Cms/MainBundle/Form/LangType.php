<?php

namespace Cms\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class LangType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('code')
            ->add('country')
            ->add('active')
            ->add('position')
            ->add('temp1')
            ->add('temp2')
            ->add('temp3')
        ;
    }

    public function getName()
    {
        return 'cms_mainbundle_langtype';
    }
}
