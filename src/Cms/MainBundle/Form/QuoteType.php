<?php

namespace Cms\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class QuoteType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => 'Name:*'))
            ->add('people', 'choice', array('label' => 'Number of People:*',
		                              	'choices' => array('1' => '1',
										                   '2' => '2',
										                   '3' => '3',
										                   '4' => '4',
										                   '5' => '5',
										                   '6' => '6',
										                   '7' => '7',
										                   '8' => '8',
										                   '9' => '9',
										                   '10' => '10',
										                   '11' => '11',
										                   '12' => '12',
										                   '13' => '13',
										                   '14' => '14',
										                   '15' => '15',
										                   '16' => '16',
										)))
            ->add('transportDate', 'date', array('widget' => 'single_text',
			                                     'format' => 'yyyy-MM-dd',
												 'label'  => 'Date of Transport:*'											 
									    ))
            ->add('transportTime', 'time', array('label' => 'Pick Up Time:*'))
            ->add('point','text', array('label' => 'Pick Up Point:*'))
            ->add('destination', 'text', array('label' => 'Destination:*'))
			
->add('single', 'choice', array(
    'choices'   => array('Return' => 'Return', 'Single' => 'Single'),
    'label' => 'Journey:'
))    
			->add('returnDate', 'date', array('widget' => 'single_text',
			                                     'format' => 'yyyy-MM-dd',
												 'label'  => 'Return Date:'											 
									    ))
            ->add('returnTime', 'time', array('label' => 'Return Time:'))
            ->add('points', 'textarea', array('label' => 'Return Drop Off Points:'))
            ->add('phone', 'text', array('label' => 'Daytime Tel No:'))
            ->add('email', 'email', array('label' => 'Email:*'))
            ->add('info', 'textarea', array('label' => 'Any Additional Information:'))
            ->add('find', 'choice', array(
    'choices'   => array('Our website' => 'Our website', 
	                     'Search engine' => 'Search engine',
	                     'Yell.com' => 'Yell.com',
	                     'Yellow Pages' => 'Yellow Pages',
	                     'Thompson Website' => 'Thompson Website',
	                     'Thompson directory' => 'Thompson directory',
	                     'Sign written bus' => 'Sign written bus',
	                     'Repeat booking' => 'Repeat booking',
	                     'Other' => 'Other'),
    'label' => 'How did you find us:*'
));
			
    }

    public function getName()
    {
        return 'cms_mainbundle_quotetype';
    }
}
