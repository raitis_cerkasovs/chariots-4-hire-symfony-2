<?php
// src/Cms/MainBundle/Entity/Article.php

namespace Cms\MainBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


//use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="lang")
 */
class Lang
{
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;  
   
   
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $name;
   
   
   
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $code;





   
   
   
   
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $country;
	
	
	
	


    /**
     * @ORM\Column(type="boolean", nullable="true", nullable="true")
     */
    protected $active;	
	




	
	


    /**
     * @ORM\Column(type="integer", nullable="true")
     */
    protected $position;	
	
		


    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $temp1;
	





    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $temp2;
	
	
	
	

    /**
     * @ORM\Column(type="text", nullable="true")
     */
    protected $temp3;




public function __toString() {

return $this->getName();}





    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set temp1
     *
     * @param string $temp1
     */
    public function setTemp1($temp1)
    {
        $this->temp1 = $temp1;
    }

    /**
     * Get temp1
     *
     * @return string 
     */
    public function getTemp1()
    {
        return $this->temp1;
    }

    /**
     * Set temp2
     *
     * @param string $temp2
     */
    public function setTemp2($temp2)
    {
        $this->temp2 = $temp2;
    }

    /**
     * Get temp2
     *
     * @return string 
     */
    public function getTemp2()
    {
        return $this->temp2;
    }

    /**
     * Set temp3
     *
     * @param text $temp3
     */
    public function setTemp3($temp3)
    {
        $this->temp3 = $temp3;
    }

    /**
     * Get temp3
     *
     * @return text 
     */
    public function getTemp3()
    {
        return $this->temp3;
    }

    /**
     * Add cat1s
     *
     * @param Cms\MainBundle\Entity\Cat1 $cat1s
     */
    public function addCat1(\Cms\MainBundle\Entity\Cat1 $cat1s)
    {
        $this->cat1s[] = $cat1s;
    }

    /**
     * Get cat1s
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCat1s()
    {
        return $this->cat1s;
    }

    /**
     * Set code
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set country
     *
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set position
     *
     * @param integer $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }
}