<?php
// src/Cms/MainBundle/Entity/Quote.php

namespace Cms\MainBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="quote")
 */
class Quote
{
	
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;



   
   
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $name;   
   
    /**
     * @ORM\Column(type="integer", nullable="true")
     */
    protected $people;  
	   
	 /**
     * @ORM\Column(type="date", nullable="true")
     */
    protected $transportDate; 
   
    /**
     * @ORM\Column(type="time", nullable="true")
     */
    protected $transportTime; 
   
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $point; 
   
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $destination;   

    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $single;
	   
	 /**
     * @ORM\Column(type="date", nullable="true")
     */
    protected $returnDate; 
   
    /**
     * @ORM\Column(type="time", nullable="true")
     */
    protected $returnTime; 

    /**
     * @ORM\Column(type="text", nullable="true")
     */
    protected $points;
      
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $email;

    /**
     * @ORM\Column(type="text", nullable="true")
     */
    protected $info;
  
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $find; 

    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $temp1;
	
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $temp2;

    /**
     * @ORM\Column(type="text", nullable="true")
     */
    protected $temp3;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set people
     *
     * @param integer $people
     */
    public function setPeople($people)
    {
        $this->people = $people;
    }

    /**
     * Get people
     *
     * @return integer 
     */
    public function getPeople()
    {
        return $this->people;
    }

    /**
     * Set transportDate
     *
     * @param datetime $transportDate
     */
    public function setTransportDate($transportDate)
    {
        $this->transportDate = $transportDate;
    }

    /**
     * Get transportDate
     *
     * @return datetime 
     */
    public function getTransportDate()
    {
        return $this->transportDate;
    }

    /**
     * Set transportTime
     *
     * @param integer $transportTime
     */
    public function setTransportTime($transportTime)
    {
        $this->transportTime = $transportTime;
    }

    /**
     * Get transportTime
     *
     * @return integer 
     */
    public function getTransportTime()
    {
        return $this->transportTime;
    }

    /**
     * Set point
     *
     * @param string $point
     */
    public function setPoint($point)
    {
        $this->point = $point;
    }

    /**
     * Get point
     *
     * @return string 
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set destination
     *
     * @param string $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * Get destination
     *
     * @return string 
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set single
     *
     * @param boolean $single
     */
    public function setSingle($single)
    {
        $this->single = $single;
    }

    /**
     * Get single
     *
     * @return boolean 
     */
    public function getSingle()
    {
        return $this->single;
    }

    /**
     * Set returnDate
     *
     * @param datetime $returnDate
     */
    public function setReturnDate($returnDate)
    {
        $this->returnDate = $returnDate;
    }

    /**
     * Get returnDate
     *
     * @return datetime 
     */
    public function getReturnDate()
    {
        return $this->returnDate;
    }

    /**
     * Set returnTime
     *
     * @param integer $returnTime
     */
    public function setReturnTime($returnTime)
    {
        $this->returnTime = $returnTime;
    }

    /**
     * Get returnTime
     *
     * @return integer 
     */
    public function getReturnTime()
    {
        return $this->returnTime;
    }

    /**
     * Set points
     *
     * @param text $points
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }

    /**
     * Get points
     *
     * @return text 
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set phone
     *
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param email $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return email 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set info
     *
     * @param text $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * Get info
     *
     * @return text 
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set find
     *
     * @param string $find
     */
    public function setFind($find)
    {
        $this->find = $find;
    }

    /**
     * Get find
     *
     * @return string 
     */
    public function getFind()
    {
        return $this->find;
    }

    /**
     * Set temp1
     *
     * @param string $temp1
     */
    public function setTemp1($temp1)
    {
        $this->temp1 = $temp1;
    }

    /**
     * Get temp1
     *
     * @return string 
     */
    public function getTemp1()
    {
        return $this->temp1;
    }

    /**
     * Set temp2
     *
     * @param string $temp2
     */
    public function setTemp2($temp2)
    {
        $this->temp2 = $temp2;
    }

    /**
     * Get temp2
     *
     * @return string 
     */
    public function getTemp2()
    {
        return $this->temp2;
    }

    /**
     * Set temp3
     *
     * @param text $temp3
     */
    public function setTemp3($temp3)
    {
        $this->temp3 = $temp3;
    }

    /**
     * Get temp3
     *
     * @return text 
     */
    public function getTemp3()
    {
        return $this->temp3;
    }
}