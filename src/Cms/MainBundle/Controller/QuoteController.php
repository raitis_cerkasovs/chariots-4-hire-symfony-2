<?php

namespace Cms\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cms\MainBundle\Entity\Quote;
use Cms\MainBundle\Form\QuoteType;

/**
 * Quote controller.
 *
 */
class QuoteController extends Controller
{
    /**
     * Lists all Quote entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('CmsMainBundle:Quote')->findAll();

        return $this->render('CmsMainBundle:Quote:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a Quote entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CmsMainBundle:Quote')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Quote entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CmsMainBundle:Quote:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),

        ));
    }

    /**
     * Displays a form to create a new Quote entity.
     *
     */
    public function newAction()
    {
        $entity = new Quote();
        $form   = $this->createForm(new QuoteType(), $entity);

        return $this->render('CmsMainBundle:Quote:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Quote entity.
     *
     */
    public function createAction()
    {
        $entity  = new Quote();
        $request = $this->getRequest();
        $form    = $this->createForm(new QuoteType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();
			
			//
			
			
			
			
			

			
			
			
			
			
			
        date_default_timezone_set("Europe/London");
        $user =  $entity->getName();
        $date = $entity->getTransportDate()->format('d-m-Y');
        $passengers = $entity->getPeople();
        $pickup = $entity->getPoint();
        $pickuptimehrs = $entity->getTransportTime()->format('G:i');
        $destination =  $entity->getDestination();
        $singleorreturn =  $entity->getSingle();
        if ($entity->getReturnDate()) $returndate =  $entity->getReturnDate()->format('d-m-Y');
        $returntimehrs =  $entity->getReturnTime()->format('G:i');
        $returndrop =  $entity->getPoints();
        $daytelephone =  $entity->getPhone();
        $email =  $entity->getEmail();
        $source =  $entity->getFind();
        $addinfo =  $entity->getInfo();
        $eol = "\n";

        $date = str_replace("-"," ",$date);
        $date = strtolower($date); 
        $date = ucwords($date);
	     
		if (isset($returndate)):         
        $returndate = str_replace("-"," ",$returndate);
        $returndate = strtolower($returndate); 
        $returndate = ucwords($returndate);
		endif;

        $message = "From : ".$email.$eol.$eol."To : ".$user.$eol.$eol."RE: Quote for minibus hire.".$eol.$eol;
        $message = $message."Please find set out below a quotation for the provision of a";
        $message = $message." minibus and driver for the conveyance of ".$passengers." passengers";
	      $message = $message." as follows.";
        $message = $message.$eol.$eol.'Job Date: ';
	    	$message = $message.$date.$eol.$eol;
        $message = $message.'Pick up point and time: ';
	    	$message = $message.$pickup;
	    	$message = $message.' at ';
	    	$message = $message.$pickuptimehrs;
        $message = $message.'Destination: ';
	    	$message = $message.$destination.$eol.$eol;
        if($singleorreturn == 'Return'){
            $message = $message.'Return journey time and date: ';   
	      		$message = $message.$returntimehrs;
 	      		$message = $message.' on the ';
				if (isset($returndate)): 
	      		$message = $message.$returndate.$eol;
				endif;
            $message = $message.$eol.'Return drop off point(s): ';
		      	$message = $message.$returndrop.$eol.$eol;    
        } else {
	          $message = $message.'No return jouney required'.$eol.$eol;
    	  }	
        if($addinfo != ''){
            $message = $message.$addinfo.$eol.$eol;
        }

        
        $message = $message.$eol.'Total cost for the above journey: (subject to availability)'.$eol.$eol;
        $message = $message.'If you have any further queries or wish to book please contact me by phone';
        $message = $message.$eol.'or email. Meanwhile we look forward to the opportunity of providing you with our service.'.$eol.$eol;
        $message = $message.'Many Thanks,'.$eol;
        $message = $message.$eol.'-----------------------------';
	  // $message = $message.$eol.$eol.$user.$br.$date.$br.$pickup.$br.$destination.$br.$daytelephone.$br.$email.$br.$source;



       /// mail('66x@inbox.lv','Online Quote',$message,'From: '.$email);
			
			
			
			
	$message_send = \Swift_Message::newInstance()
        ->setSubject('Quote Chariots4hire')
        ->setFrom($email)
        ->setTo('admin@chariots4hire.co.uk')
        ->setBody($message);
    $this->get('mailer')->send($message_send);
			
			
			
		
			
			//

             return $this->render('CmsMainBundle:Quote:thanks.html.twig');
            
        }

        return $this->render('CmsMainBundle:Quote:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Quote entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CmsMainBundle:Quote')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Quote entity.');
        }

        $editForm = $this->createForm(new QuoteType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CmsMainBundle:Quote:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Quote entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CmsMainBundle:Quote')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Quote entity.');
        }

        $editForm   = $this->createForm(new QuoteType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('quote_edit', array('id' => $id)));
        }

        return $this->render('CmsMainBundle:Quote:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Quote entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('CmsMainBundle:Quote')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Quote entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('quote'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
	



}
