<?php
namespace Cms\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cms\MainBundle\Entity\Cat1;
use Cms\MainBundle\Form\Cat1Type;

use Cms\MainBundle\Entity\Article;
use Cms\MainBundle\Controller\ArticleController;


/**
 * Cat1 controller.
 *
 */
class Cat1Controller extends Controller
{

	
    /**
     * Lists all Cat1 entities.
     *
     */
    public function indexAction()
    {
        $entities = $this->getDoctrine()->getRepository('CmsMainBundle:Cat1')->findBy(array(), array('position' => 'asc')); 
        $locale = $this->get('session')->getLocale();

        return $this->render('CmsMainBundle:Cat1:index.html.twig', array(
            'entities' => $entities,
	        'locale' => $locale
        ));
    }


    /**
     * Displays a form to create a new Cat1 entity.
     *
     */
    public function newAction()
    {
        $entity = new Cat1();
        $form   = $this->createFormBuilder($entity)
			  ->add('name')
			  ->add('design')
			  ->add('active')
              ->getForm();
        return $this->render('CmsMainBundle:Cat1:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Creates a new Cat1 entity.
     *
     */
    public function createAction()
    {
        $entity_cat = new Cat1();
        $entity_art = new Article();
        $articleController = new ArticleController();
				
		$entity_cat->setPosition($this->findPosition());

        $request = $this->getRequest();
        $form   = $this->createFormBuilder($entity_cat)
			  ->add('name')
			  ->add('design')
			  ->add('active')
              ->getForm();
        $form->bindRequest($request);

        if ($form->isValid()) {
			
            $em = $this->getDoctrine()->getEntityManager();
			
			$entity_art->setBody('You can edit this page by clicking "'.$entity_cat->getName().'" in menu.');
			$entity_art->setAutoslag($articleController->formUrl('auto-slag-'.$entity_cat->getName()));
			$entity_art->setSlag($articleController->formUrl($entity_cat->getName()));
			$entity_art->setTitle($entity_cat->getName());
            $entity_art->setPublishDate(new \DateTime('now'));
			$em->persist($entity_art);
            $em->flush();

			$entity_cat->setArticle($entity_art);
            $entity_cat->setLangcode($this->get('session')->getLocale()); 
			$em->persist($entity_cat);
            $em->flush();


            return $this->redirect($this->generateUrl('cat1'));
            
        }

        return $this->render('CmsMainBundle:Cat1:new.html.twig', array(
            'entity' => $entity_cat,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Cat1 entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository('CmsMainBundle:Cat1')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cat1 entity.');
        }

        $editForm = $this->createFormBuilder($entity)
			  ->add('name')
			  ->add('design')
			  ->add('active')
              ->getForm();
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CmsMainBundle:Cat1:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Cat1 entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CmsMainBundle:Cat1')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cat1 entity.');
        }

        $editForm = $this->createFormBuilder($entity)
			  ->add('name')
			  ->add('design')
			  ->add('active')
              ->getForm();
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('cat1'));
        }

        return $this->render('CmsMainBundle:Cat1:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Cat1 entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('CmsMainBundle:Cat1')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Cat1 entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cat1'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }




/////////////////////////////////////////////////// position

    public function findPosition()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $entities = $em->getRepository('CmsMainBundle:Cat1')->findAll();
		$cup = 0;
		foreach ($entities as $entity):
         if ($entity->getPosition() > $cup):
	       $cup = $entity->getPosition();
		 endif; 
		endforeach;
        $cup++;
		
		return $cup;
    }   



    public function downAction($id)
    {
$locale = $this->get('session')->getLocale();
$entities = $this->getDoctrine()->getRepository('CmsMainBundle:Cat1')->findBy(array('langcode' => $locale), array('position' => 'asc')); 

// get id next
        $flag = false;
		foreach ($entities as $entity):
		if ($flag):
		   $id_next = $entity->getId();
		   $flag = false;
		 endif; 
         if ($entity->getId() == $id):
		   $flag = true;
		 endif; 
		endforeach;

// swap value


if (isset($id_next)): 
        $em = $this->getDoctrine()->getEntityManager();
      
	    $entity_old = $em->getRepository('CmsMainBundle:Cat1')->find($id);
        $entity_new = $em->getRepository('CmsMainBundle:Cat1')->find($id_next);   
		
		$old = $entity_old->getPosition();    
		$new = $entity_new->getPosition();    
  
        $entity_old->setPosition($new);      
	    $em->persist($entity_old);
        $em->flush();

        $entity_new->setPosition($old);
	    $em->persist($entity_new);
        $em->flush();
  
endif;
		
        return $this->redirect($this->generateUrl('cat1'));
    }  






    public function upAction($id)
    {
$locale = $this->get('session')->getLocale();
$entities = $this->getDoctrine()->getRepository('CmsMainBundle:Cat1')->findBy(array('langcode' => $locale), array('position' => 'asc')); 

// get prev id 
        $flag = true;
		foreach ($entities as $key => $entity):
         if ($entity->getId() == $id and $flag and $key != 0):
           $id_up = $entities[$key-1]->getId();
		   $flag = false;
	 	 endif; 
		endforeach;


if (isset($id_up)): 
        $em = $this->getDoctrine()->getEntityManager();
      
	    $entity_old = $em->getRepository('CmsMainBundle:Cat1')->find($id);
        $entity_new = $em->getRepository('CmsMainBundle:Cat1')->find($id_up);   
		
		$old = $entity_old->getPosition();    
		$new = $entity_new->getPosition();    
  
        $entity_old->setPosition($new);      
	    $em->persist($entity_old);
        $em->flush();

        $entity_new->setPosition($old);
	    $em->persist($entity_new);
        $em->flush();
  
endif;
		
        return $this->redirect($this->generateUrl('cat1'));
    } 

/////   sidebar
    public function sidebarAction() {	

	 $entities = $this->getDoctrine()->getRepository('CmsMainBundle:Cat1')->findBy(array('active'   =>  1), array('position' => 'asc')); 
     $locale = $this->get('session')->getLocale();
	 $path = $_SERVER['PHP_SELF']; 
	 
	 return $this->render('CmsMainBundle:Cat1:sidebar.html.twig', array('entities' => $entities, 
	                                                                        'path' => $path,
	                                                                        'locale' => $locale
																	    ));
    }

/////   sidebar Old
    public function sidebarOldAction() {	

		$entity_a = $this->getDoctrine()->getEntityManager()->getRepository('CmsMainBundle:Article')->find(2);
		$entity_b = $this->getDoctrine()->getEntityManager()->getRepository('CmsMainBundle:Article')->find(1);

	 
	 return $this->render('CmsMainBundle:Cat1:sidebarOld.html.twig', array('entity_a' => $entity_a, 
                                                                           'entity_b' => $entity_b
																	    ));
    }
	
	
/////// sidebar bottom	
    public function sidebarBotomAction()
    {
	$entities = $this->getDoctrine()->getRepository('CmsMainBundle:Cat1')->findBy(array(), array('position' => 'asc')); 

	   return $this->render('CmsMainBundle:Cat1:sidebarBotom.html.twig', array(
            'entities' => $entities
        ));	
	}

}
