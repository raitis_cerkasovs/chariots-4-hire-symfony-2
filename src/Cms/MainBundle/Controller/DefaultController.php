<?php
namespace Cms\MainBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cms\MainBundle\Entity\Article;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

  use Symfony\Component\Form\AbstractType;
  use Symfony\Component\Form\FormBuilder;

class DefaultController extends Controller
{
	
    
    public function indexAction()
    {
		
	  if ($this->get('session')->getLocale() == 'gp' )
	      $this->setDefLang();
		  
	 
	  $locale = $this->get('session')->getLocale();

	  $entities_cat1 = $this->getDoctrine()->getRepository('CmsMainBundle:Cat1')->findBy(array('active'   =>  1, 
	                                                                                           'langcode' => $locale),
	                                                                                     array('position' => 'asc',
																				         )); 	
	  
	  if (!$entities_cat1)	  
        return $this->redirect($this->generateUrl('default_no_menu'));
		 	  
	  if ($entities_cat1[0]->getArticle()->getSlagtrue() < 1)
	    return $this->redirect($this->generateUrl('article_show', array('id'   => $entities_cat1[0]->getArticle()->getId(),
                                                                        'slug' => $entities_cat1[0]->getArticle()->getAutoslag()
																		)));
	  else
	    return $this->redirect($this->generateUrl('article_show', array('id'   => $entities_cat1[0]->getArticle()->getId(),
                                                                        'slug' => $entities_cat1[0]->getArticle()->getSlag()
																		)));
	
    }
	
      
	  
	   public function setDefLang()
    {
	     
	  // find first and default language 
	  $entities_lang = $this->getDoctrine()->getRepository('CmsMainBundle:Lang')->findBy(array('active'   =>  1),
	                                                                               array('position' => 'asc')); 	  	 
	  if (!$entities_lang)
	     throw new \Exception('No languages detected');	

	  // set default language
	  $this->get('session')->setLocale($entities_lang[0]->getCode());



 // check for autodiscover locale
 $entities_util = $this->getDoctrine()->getRepository('CmsMainBundle:Utility')->findBy(array('id'   =>  1, 'active' => 1)); 		
    
 if (isset($entities_util[0])):

	 // find user locale and make lowercase	
     $lo = strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']);
	   
     // find if locale could be en	 
	 if (strpos($lo, 'gb') !== false or strpos($lo, 'us') !== false or strpos($lo, 'au') !== false or strpos($lo, 'en') !== false)
	    $setengl = true; 		      

		 

	 // go trough locales 
	   foreach ($entities_lang as $entity):
	   
	 // set any en locale
	      if (isset($setengl)) {
			  if ($entity->getCode() == 'gb' or $entity->getCode() == 'us' or $entity->getCode() == 'au') {
		        $this->get('session')->setLocale($entity->getCode());
			  }
		  }
	 
	 //	set locale who mutches	  			  
	      if (strpos($lo, $entity->getCode()) !== false)
		     $this->get('session')->setLocale($entity->getCode());
			 
	   endforeach;
 
 endif;

}
	

 
   
      public function nomenuAction()
    {
        return $this->render('CmsMainBundle:Default:nomenu.html.twig');
	}
	
   
      public function googleChartAction()
    {
		$em = $this->getDoctrine()->getEntityManager();
        $entities = $em->getRepository('CmsMainBundle:Quote')->findAll();
		$result = array('our_website' => 0,
		                'search_engine' => 0,
		                'yellow_pages' => 0,
		                'thompson_website' => 0,
		                'thompson_directory' => 0,
		                'sign_written_bus' => 0,
		                'repeat_booking' => 0,
		                'yell_com' => 0,
		                'other' => 0,							
		               );
					   
		foreach ($entities as $entity):
		    if ($entity->getFind() == 'Our website')
			        $result['our_website']++;

		    if ($entity->getFind() == 'Search engine')
			        $result['search_engine']++;

		    if ($entity->getFind() == 'Yellow Pages')
			        $result['yellow_pages']++;

		    if ($entity->getFind() == 'Thompson Website')
			        $result['thompson_website']++;

		    if ($entity->getFind() == 'Thompson directory')
			        $result['thompson_directory']++;

		    if ($entity->getFind() == 'Sign written bus')
			        $result['sign_written_bus']++;

		    if ($entity->getFind() == 'Repeat booking')
			        $result['repeat_booking']++;

		    if ($entity->getFind() == 'Yell.com')
			        $result['yell_com']++;

		    if ($entity->getFind() == 'Other')
			        $result['other']++;
					
		endforeach;
		
        return $this->render('CmsMainBundle:Default:googleChart.html.twig', array('result' => $result));
	}

	
}