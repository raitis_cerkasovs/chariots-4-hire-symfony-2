<?php

namespace Cms\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cms\MainBundle\Entity\Cat2;
use Cms\MainBundle\Form\Cat2Type;

use Doctrine\ORM\EntityRepository;

use Cms\MainBundle\Entity\Article;



/**
 * Cat2 controller.
 *
 */
class Cat2Controller extends Controller
{

    /**
     * Displays a form to create a new Cat2 entity.
     *
     */
	 
	 
    public function newAction()
    {
		
		
        $entity = new Cat2();
		$locale = $this->get('session')->getLocale();

		$form   = $this->createFormBuilder($entity)
			  ->add('cat1', 'entity', array(
				                           'class'         => 'CmsMainBundle:Cat1',
                                           'query_builder' => function(EntityRepository $er) use ($locale) {
		                                                                return $er->createQueryBuilder('p')
																		          ->where("p.langcode = :locale")
																				  ->setParameter('locale', $locale); 
										                      }
                                                            ))
			  ->add('name')
			  ->add('design')
			  ->add('active')
			  ->add('ajaxtrue')
              ->getForm();
			  
			  
        return $this->render('CmsMainBundle:Cat2:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
		
    }

    /**
     * Creates a new Cat2 entity.
     *
     */
    public function createAction()
    {
        $entity  = new Cat2();
        $entity_art = new Article();
        $articleController = new ArticleController();

		$entity->setPosition($this->findPosition());

		$request = $this->getRequest();
		$locale = $this->get('session')->getLocale();

		$form   = $this->createFormBuilder($entity)
			  ->add('cat1', 'entity', array(
				                           'class'         => 'CmsMainBundle:Cat1',
                                           'query_builder' => function(EntityRepository $er) use ($locale) {
		                                                                return $er->createQueryBuilder('p')
																		          ->where("p.langcode = :locale")
																				  ->setParameter('locale', $locale); 
										                      }
                                                            ))
			  ->add('name')
			  ->add('design')
			  ->add('active')
			  ->add('ajaxtrue')
              ->getForm();
         $form->bindRequest($request);
		 
		$entity->setLangcode($locale);


        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
			
			$entity_art->setBody('You can edit this page by clicking "'.$entity->getName().'" in submenu.');
			$entity_art->setAutoslag($articleController->formUrl('auto-slag-'.$entity->getName()));
			$entity_art->setSlag($articleController->formUrl($entity->getName()));
			$entity_art->setTitle($entity->getName());
            $entity_art->setPublishDate(new \DateTime('now'));
			$em->persist($entity_art);
            $em->flush();

			$entity->setArticle($entity_art);
            $entity->setLangcode($this->get('session')->getLocale()); 
			$em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('cat1'));
            
        }

        return $this->render('CmsMainBundle:Cat2:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Cat2 entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository('CmsMainBundle:Cat2')->find($id);
        $locale = $this->get('session')->getLocale();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cat2 entity.');
        }



		$form   = $this->createFormBuilder($entity)
			  ->add('cat1', 'entity', array(
				                           'class'         => 'CmsMainBundle:Cat1',
                                           'query_builder' => function(EntityRepository $er) use ($locale) {
		                                                                return $er->createQueryBuilder('p')
																		          ->where("p.langcode = :locale")
																				  ->setParameter('locale', $locale); 
										                      }
                                                            ))
			  ->add('name')
			  ->add('design')
			  ->add('active')
			  ->add('ajaxtrue')
              ->getForm();      
			  
	    $deleteForm = $this->createDeleteForm($id);

        return $this->render('CmsMainBundle:Cat2:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Cat2 entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $locale = $this->get('session')->getLocale();
        $entity = $em->getRepository('CmsMainBundle:Cat2')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cat2 entity.');
        }

		$form   = $this->createFormBuilder($entity)
			  ->add('cat1', 'entity', array(
				                           'class'         => 'CmsMainBundle:Cat1',
                                           'query_builder' => function(EntityRepository $er) use ($locale) {
		                                                                return $er->createQueryBuilder('p')
																		          ->where("p.langcode = :locale")
																				  ->setParameter('locale', $locale); 
										                      }
                                                            ))
			  ->add('name')
			  ->add('design')
			  ->add('active')
			  ->add('ajaxtrue')
              ->getForm(); 
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('cat1'));
        }

        return $this->render('CmsMainBundle:Cat2:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Cat2 entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('CmsMainBundle:Cat2')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Cat2 entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cat1'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
	
/////////////////////////////////////////////////// position

    public function findPosition()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $entities = $em->getRepository('CmsMainBundle:Cat2')->findAll();
		$cup = 0;
		foreach ($entities as $entity):
         if ($entity->getPosition() > $cup):
	       $cup = $entity->getPosition();
		 endif; 
		endforeach;
        $cup++;
		
		return $cup;
    }  
	
############################################################


    public function downAction($id, $cat1Id)
    {
$locale = $this->get('session')->getLocale();
$entities = $this->getDoctrine()->getRepository('CmsMainBundle:Cat2')->findBy(array(

'langcode' => $locale,
'cat1' => $cat1Id



), array('position' => 'asc')); 

// get id next
        $flag = false;
		foreach ($entities as $entity):
		if ($flag):
		   $id_next = $entity->getId();
		   $flag = false;
		 endif; 
         if ($entity->getId() == $id):
		   $flag = true;
		 endif; 
		endforeach;

// swap value


if (isset($id_next)): 
        $em = $this->getDoctrine()->getEntityManager();
      
	    $entity_old = $em->getRepository('CmsMainBundle:Cat2')->find($id);
        $entity_new = $em->getRepository('CmsMainBundle:Cat2')->find($id_next);   

if ($entity_old->getCat1() == $entity_new->getCat1()): 
		
		$old = $entity_old->getPosition();    
		$new = $entity_new->getPosition();    
  
        $entity_old->setPosition($new);      
	    $em->persist($entity_old);
        $em->flush();

        $entity_new->setPosition($old);
	    $em->persist($entity_new);
        $em->flush();
  
endif;
endif;
		
        return $this->redirect($this->generateUrl('cat1'));
    }  






    public function upAction($id, $cat1Id)
    {

$locale = $this->get('session')->getLocale();
$entities = $this->getDoctrine()->getRepository('CmsMainBundle:Cat2')->findBy(array(

'langcode' => $locale,
'cat1' => $cat1Id

), array('position' => 'asc')); 

// get prev id 
        $flag = true;
		foreach ($entities as $key => $entity):
         if ($entity->getId() == $id and $flag and $key != 0):
           $id_up = $entities[$key-1]->getId();
		   $flag = false;
	 	 endif; 
		endforeach;


if (isset($id_up)): 
        $em = $this->getDoctrine()->getEntityManager();
      
	    $entity_old = $em->getRepository('CmsMainBundle:Cat2')->find($id);
        $entity_new = $em->getRepository('CmsMainBundle:Cat2')->find($id_up);  
		
		$old = $entity_old->getPosition();    
		$new = $entity_new->getPosition();    
  
        $entity_old->setPosition($new);      
	    $em->persist($entity_old);
        $em->flush();

        $entity_new->setPosition($old);
	    $em->persist($entity_new);
        $em->flush();
  
endif;
		
        return $this->redirect($this->generateUrl('cat1'));
    } 


}
