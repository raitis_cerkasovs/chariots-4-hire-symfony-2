<?php
namespace Cms\MainBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
	
    
    public function adminAction()
    {
	$entities = $this->getDoctrine()->getRepository('CmsMainBundle:Cat1')->findBy(array(), array('position' => 'asc')); 

	   return $this->render('CmsMainBundle:Admin:admin.html.twig', array(
            'entities' => $entities
        ));	
	}

}
