<?php
namespace Cms\MainBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cms\MainBundle\Entity\Lang;
use Cms\MainBundle\Form\LangType;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\Extension\Core\Validator; 


use Symfony\Component\Form\FormInterface; 
use Symfony\Component\Form\FormValidatorInterface; 
use Symfony\Component\Form\FormError; 


/**
 * Lang controller.
 *
 */
class LangController extends Controller
{
    /**
     * Lists all Lang entities.
     *
     */
    public function indexAction()
    {
		
$entity_util = $this->getDoctrine()->getEntityManager()->getRepository('CmsMainBundle:Utility')->find(1);
$entities    = $this->getDoctrine()->getRepository('CmsMainBundle:Lang')->findBy(array(), array('position' => 'asc'));

        return $this->render('CmsMainBundle:Lang:index.html.twig', array(
            'entities' => $entities,
            'entity_util' => $entity_util,
        ));
    }

  


	
	
    public function newAction()
    {	
        $entity = new Lang();

        $form   = $this->createFormBuilder($entity)
		      ->add('country', 'country')
			  ->add('name')
			  ->add('code')
			  ->add('active')
              ->getForm();

        return $this->render('CmsMainBundle:Lang:new.html.twig', array(
            'form' => $form->createView(),
        ));
		
	}
	
	
	
    public function createAction()
    {
        $entity  = new Lang();
		$entity->setPosition($this->findPosition());
        $request = $this->getRequest();
        $form    = $this->createFormBuilder($entity)
		      ->add('country', 'country')
			  ->add('name')
			  ->add('code')
			  ->add('active')
              ->getForm();
		
        $form->bindRequest($request);
           
			
			// validate
		    $validator = $this->get('validator');
            $errors = $validator->validate($entity);

	
			
        if (count($errors) == 0) {
			  $entity->setCode(strtolower($entity->getCountry()));
			
              $em = $this->getDoctrine()->getEntityManager();
              $em->persist($entity);
              $em->flush();

            return $this->redirect($this->generateUrl('lang'));           
        }
		

        return $this->render('CmsMainBundle:Lang:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }


    /**
     * Displays a form to edit an existing Lang entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CmsMainBundle:Lang')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lang entity.');
        }

        $editForm = $this->createFormBuilder($entity)
		      ->add('country', 'country')
			  ->add('name')
			  ->add('code')
			  ->add('active')
              ->getForm();

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CmsMainBundle:Lang:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Lang entity.
     *
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('CmsMainBundle:Lang')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lang entity.');
        }

        $editForm = $this->createFormBuilder($entity)
		      ->add('country', 'country')
			  ->add('name')
			  ->add('code')
			  ->add('active')
              ->getForm();
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);
			
			// validate
		    $validator = $this->get('validator');
            $errors = $validator->validate($entity);

        if (count($errors) == 0) {
			$entity->setCode(strtolower($entity->getCountry()));
			
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('lang'));
        }



        return $this->render('CmsMainBundle:Lang:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Lang entity.
     *
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('CmsMainBundle:Lang')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Lang entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('lang'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm();
    }



/////////////////////////////////////////////////// position

    public function findPosition()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $entities = $em->getRepository('CmsMainBundle:Lang')->findAll();
		$cup = 0;
		foreach ($entities as $entity):
         if ($entity->getPosition() > $cup):
	       $cup = $entity->getPosition();
		 endif; 
		endforeach;
        $cup++;
		
		return $cup;
    }   



    public function downAction($id)
    {
$entities = $this->getDoctrine()->getRepository('CmsMainBundle:Lang')->findBy(array(), array('position' => 'asc')); 

// get id next
        $flag = false;
		foreach ($entities as $entity):
		if ($flag):
		   $id_next = $entity->getId();
		   $flag = false;
		 endif; 
         if ($entity->getId() == $id):
		   $flag = true;
		 endif; 
		endforeach;

// swap value


if (isset($id_next)): 
        $em = $this->getDoctrine()->getEntityManager();
      
	    $entity_old = $em->getRepository('CmsMainBundle:Lang')->find($id);
        $entity_new = $em->getRepository('CmsMainBundle:Lang')->find($id_next);   
		
		$old = $entity_old->getPosition();    
		$new = $entity_new->getPosition();    
  
        $entity_old->setPosition($new);      
	    $em->persist($entity_old);
        $em->flush();

        $entity_new->setPosition($old);
	    $em->persist($entity_new);
        $em->flush();
  
endif;
		
        return $this->redirect($this->generateUrl('lang'));
    }  






    public function upAction($id)
    {
$entities = $this->getDoctrine()->getRepository('CmsMainBundle:Lang')->findBy(array(), array('position' => 'asc')); 

// get prev id 
        $flag = true;
		foreach ($entities as $key => $entity):
         if ($entity->getId() == $id and $flag and $key != 0):
           $id_up = $entities[$key-1]->getId();
		   $flag = false;
	 	 endif; 
		endforeach;


if (isset($id_up)): 
        $em = $this->getDoctrine()->getEntityManager();
      
	    $entity_old = $em->getRepository('CmsMainBundle:Lang')->find($id);
        $entity_new = $em->getRepository('CmsMainBundle:Lang')->find($id_up);   
		
		$old = $entity_old->getPosition();    
		$new = $entity_new->getPosition();    
  
        $entity_old->setPosition($new);      
	    $em->persist($entity_old);
        $em->flush();

        $entity_new->setPosition($old);
	    $em->persist($entity_new);
        $em->flush();
  
endif;
		
        return $this->redirect($this->generateUrl('lang'));
    } 

///////   sidebar

    public function sidebarLangAction() {	
    
	 $entities = $this->getDoctrine()->getRepository('CmsMainBundle:Lang')->findBy(array('active'   =>  1),
	                                                                               array('position' => 'asc')); 
	 $locale = $this->get('session')->getLocale();
	
	 return $this->render('CmsMainBundle:Lang:sidebarLang.html.twig', array('entities' => $entities,
	                                                                        'locale'   => $locale,	
																			));
    }



    public function setLangAction($code)
    {
        $this->get('session')->setLocale($code);

        return $this->redirect($this->generateUrl('_welcome'));
    }
	
	
	
	    public function changeDefLangAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository('CmsMainBundle:Utility')->find(1);

         if ($entity->getActive(1))		 
		  $entity->setActive(0);
		 else
          $entity->setActive(1);

		    $em->persist($entity);
            $em->flush();
        
		return $this->redirect($this->generateUrl('lang'));

    }
}



