<?php
// src/Cms/DemoBundle/Entity/Document.php
namespace Cms\FileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="document")
 * @ORM\HasLifecycleCallbacks
 */
class Document
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $path;

    /**
     * @Assert\File(maxSize="6000000")
     */
    public $file;
   
    /**
     * @ORM\Column(type="text", nullable="true")
     */
    protected $body;
 
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $title;	
	   
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $thumbnail;
   
    /**
     * @ORM\Column(type="integer", nullable="true")
     */
    protected $flag;

    /**
     * @ORM\Column(type="boolean", nullable="true")
     */
    protected $active;

    /**
     * @ORM\Column(type="datetime", nullable="true")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $publish_date;

    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $temp1;
	
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    protected $temp2;

    /**
     * @ORM\Column(type="text", nullable="true")
     */
    protected $temp3;

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            // do whatever you want to generate a unique name
            $this->path = uniqid().'.'.$this->file->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move($this->getUploadRootDir(), $this->path);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }


    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/documents';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set body
     *
     * @param text $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * Get body
     *
     * @return text 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * Get thumbnail
     *
     * @return string 
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set publish_date
     *
     * @param datetime $publishDate
     */
    public function setPublishDate($publishDate)
    {
        $this->publish_date = $publishDate;
    }

    /**
     * Get publish_date
     *
     * @return datetime 
     */
    public function getPublishDate()
    {
        return $this->publish_date;
    }

    /**
     * Set temp1
     *
     * @param string $temp1
     */
    public function setTemp1($temp1)
    {
        $this->temp1 = $temp1;
    }

    /**
     * Get temp1
     *
     * @return string 
     */
    public function getTemp1()
    {
        return $this->temp1;
    }

    /**
     * Set temp2
     *
     * @param string $temp2
     */
    public function setTemp2($temp2)
    {
        $this->temp2 = $temp2;
    }

    /**
     * Get temp2
     *
     * @return string 
     */
    public function getTemp2()
    {
        return $this->temp2;
    }

    /**
     * Set temp3
     *
     * @param text $temp3
     */
    public function setTemp3($temp3)
    {
        $this->temp3 = $temp3;
    }

    /**
     * Get temp3
     *
     * @return text 
     */
    public function getTemp3()
    {
        return $this->temp3;
    }
	

    /**
     * Set flag
     *
     * @param integer $flag
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;
    }

    /**
     * Get flag
     *
     * @return integer 
     */
    public function getFlag()
    {
        return $this->flag;
    }
	
}